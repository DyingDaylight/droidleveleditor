package model;

/**
 * Created by KaterinaG on 05.08.2014.
 * Settings for level.
 */
public class FieldSettings {

    private int fieldWidth = 1920;
    private int fieldHeight = 1080;

    private int roundTo = 10;
    private int scaleFactor = 2;
    private boolean showVertexesIndex = true;
    private boolean showPlatformIndex = true;

    public void setFieldHeight(int fieldHeight) {
        this.fieldHeight = fieldHeight;
    }

    public void setFieldWidth(int fieldWidth) {
        this.fieldWidth = fieldWidth;
    }

    public int getFieldHeight() {
        return fieldHeight;
    }

    public int getFieldWidth() {
        return fieldWidth;
    }

    public void setRoundTo(int roundTo) {
        this.roundTo = roundTo;
    }

    public int getRoundTo() {
        return roundTo;
    }

    public boolean showVertexesIndex() {
        return showVertexesIndex;
    }

    public void setShowVertexesIndex(boolean showVertexesIndex){
        this.showVertexesIndex = showVertexesIndex;
    }

    public void setShowPlatformIndex(boolean showPlatformIndex) {
        this.showPlatformIndex = showPlatformIndex;
    }

    public void setScaleFactor(int scaleFactor) { this.scaleFactor = scaleFactor; }

    public int getScaleFactor() { return scaleFactor; }

    public boolean isShowPlatformIndex() {
        return showPlatformIndex;
    }

    public static FieldSettings copy(FieldSettings settings) {
        FieldSettings newSettings = new FieldSettings();
        newSettings.fieldHeight = settings.fieldHeight;
        newSettings.fieldWidth = settings.fieldWidth;
        newSettings.roundTo = settings.roundTo;
        newSettings.showVertexesIndex = settings.showVertexesIndex;
        newSettings.showPlatformIndex = settings.showPlatformIndex;
        newSettings.scaleFactor = settings.scaleFactor;
        return newSettings;
    }


}


