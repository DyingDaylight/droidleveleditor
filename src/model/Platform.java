package model;

import manager.SaveManager;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.*;
import java.util.List;

/**
 * Created by KaterinaG on 04.08.2014.
 * Platform is a number of angles.
 * Platform is finished if first angle is equal to the last angle.
 */
public class Platform {

    java.util.List<Point> points = new ArrayList<Point>();
    Ellipse2D indexGraphic = new Ellipse2D.Double();
    HashMap<String, String> properties = new HashMap<>();
    private Ellipse2D graphicIndex;


    public Platform(){
        SaveManager saveManager = new SaveManager();
        properties = saveManager.getPresets(false);
    }

    public void addPoint(Point point, int index) {
        if (index == -1)
            points.add(point);
        else
            points.add(index + 1, point);
    }

    public int getAnglesNum(){
        return points.size() - 1;
    }

    public Point getPoint(int index) {
        if (index < 0 || index > points.size() - 1) {
            return new Point();
        }
        return points.get(index);
    }

    public boolean isStartPoint(Point point) {
        if (points == null || points.isEmpty())
            return  false;

        return (getPoint(0).x == point.x && getPoint(0).y == point.y);
    }

    public int getPointIndex(Point point) {
        for (int i = 0; i < points.size(); i++) {
            if (point.x == points.get(i).x && point.y == points.get(i).y)
                return i;
        }
        return -1;
    }

    public boolean hasPoint(Point point) {
        for (Point p: points) {
            if (p.x == point.x && p.y == point.y) {
                return  true;
            }
        }
        return false;
    }

    public void deletePoint(Point point) {
        for (Point p: points) {
            if (p.x == point.x && p.y == point.y) {
                points.remove(p);
                break;
            }
        }
    }

    public boolean isPlatformFinished() {
        if (points == null || points.isEmpty())
            return  false;

        return (points.size() > 1 &&
                getPoint(0).x == getPoint(points.size() - 1).x && getPoint(0).y == getPoint(points.size() - 1).y);
    }

    public Point getCenter() {
        int xSum = 0;
        int ySum = 0;
        for (Point p: points) {
            xSum += p.x;
            ySum += p.y;
        }
        Point point = new Point();
        point.x = xSum / points.size();
        point.y = ySum / points.size();
        return point;
    }

    public Ellipse2D getIndexGraphic() {
        return indexGraphic;
    }

    public HashMap<String, String> getProperties() {
        return properties;
    }

    public void setProperties(HashMap<String, String> properties) {
        this.properties = properties;
    }

    public void scalePolygon(int oldScaleFactor, int newScaleFactor) {
        for (Point point : points){
            point.setLocation(point.getX() * oldScaleFactor / newScaleFactor,
                    point.getY() * oldScaleFactor / newScaleFactor);
        }

        indexGraphic.setFrame(indexGraphic.getX() * oldScaleFactor / newScaleFactor,
                indexGraphic.getY() * oldScaleFactor / newScaleFactor,
                indexGraphic.getWidth() * oldScaleFactor / newScaleFactor,
                indexGraphic.getHeight() * oldScaleFactor / newScaleFactor);
    }

    public Platform copy(int scaleFactor, int newScaleFactor) {
        Platform platform = new Platform();
        platform.setProperties(properties);
        platform.setPoints(platform.copyPoints(points));
        platform.setGraphicIndex(indexGraphic);
        platform.scalePolygon(scaleFactor, newScaleFactor);
        return  platform;
    }

    private List<Point> copyPoints(List<Point> points) {
        List<Point> copyList = new ArrayList<Point>();
        for (Point point : points) {
            Point copy = new Point(point.x, point.y);
            copyList.add(copy);
        }
        return copyList;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setGraphicIndex(Ellipse2D graphicIndex) {
        this.graphicIndex = graphicIndex;
    }

    public Ellipse2D getGraphicIndex() {
        return graphicIndex;
    }
}
