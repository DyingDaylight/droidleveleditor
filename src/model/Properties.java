package model;

import manager.SaveManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by kettricken on 07.08.2014.
 */
public class Properties {

    FieldSettings fieldSettings;

    ArrayList<Platform> platformList;
    private HashMap<String, String> levelSettings;

    ArrayList<Point> keys = new ArrayList<Point>();
 //   private ArrayList<Point> keyPoints;

    //Point key = new Point();

    public Properties(){
        fieldSettings = new FieldSettings();
        platformList = new ArrayList<Platform>();
        SaveManager saveManager = new SaveManager();
        levelSettings = saveManager.getPresets(true);
    }

    public FieldSettings getFieldSettings() {
        return fieldSettings;
    }

    public void setFieldSettings(FieldSettings fieldSettings) {
        this.fieldSettings = fieldSettings;
    }

    public ArrayList<Platform> getPlatformList() {
        return platformList;
    }

    public void setLevelSettings(HashMap<String, String> levelSettings) {
        this.levelSettings = levelSettings;
        Iterator it = levelSettings.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            System.out.println(pairs.getKey() + " = " + pairs.getValue());
        }
    }

    public HashMap<String, String> getLevelSettings() {
        return levelSettings;
    }

  //  public Point getKeyPoint() {
 //       return key;
  //  }

    public void setKeyPoint(Point startPoint) {
        Point key = new Point();
        key.x = startPoint.x;
        key.y = startPoint.y;
        keys.add(key);
    }

    public void setStartPoint(int i, int i1) {
        Point key = new Point();
        key.x = i;
        key.y = i1;
        keys.add(key);
    }

    public Properties copy() {
        Properties properties = new Properties();
        ArrayList<Platform> copiedPlatforms = new ArrayList<Platform>();
        for (Platform platform : platformList) {
            Platform copy = platform.copy(fieldSettings.getScaleFactor(), 1);
            copiedPlatforms.add(copy);
        }
        properties.setPlatformList(copiedPlatforms);
        properties.setLevelSettings(levelSettings);
        properties.setFieldSettings(FieldSettings.copy(fieldSettings));
        ArrayList<Point> tmpKeys = new ArrayList<>();
        for (Point key : keys) {
            Point startPoint = new Point((int) (key.getX() * fieldSettings.getScaleFactor()),
                    (int) (key.getY() * fieldSettings.getScaleFactor()));
            tmpKeys.add(startPoint);
        }
        properties.setKeyPoints(tmpKeys);
        return properties;
    }

    public void setPlatformList(ArrayList<Platform> platformList) {
        this.platformList = platformList;
    }

    public void setKeyPoints(ArrayList<Point> keyPoints) {
        this.keys.clear();
        keys.addAll(keyPoints);
    }

    public ArrayList<Point> getKeyPoints() {
        if (keys == null)
            keys = new ArrayList<Point>();
        return keys;
    }

    public void clearKeyPoints() {
        if (keys != null)
            keys.clear();
    }
}
