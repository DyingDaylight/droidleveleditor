package manager;

import model.FieldSettings;
import model.Platform;
import model.Properties;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.awt.*;
import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;

/**
 * Created by kettricken on 07.08.2014.
 */
public class SaveManager {

    private final String FIELD_PROPERTIES = "FieldProperties";
    private final String PLATFORM_PROPERTIES = "PlatformsProperties";
    private final String LEVEL_PROPERTIES = "LevelProperties";

    private final String divider = "=";

    public SaveManager() {}

    public boolean save(Properties properties, String fileToSave) {

        FieldSettings settings = properties.getFieldSettings();
        List<Platform> platforms = properties.getPlatformList();

        JSONObject propertiesJSON = new JSONObject();
        JSONObject fieldProperties = new JSONObject();
        JSONArray platformsProperties = new JSONArray();
        JSONObject levelProperties = new JSONObject(properties.getLevelSettings());

        try {
            JSONArray keyPoints = new JSONArray();
            for (Point point : properties.getKeyPoints()) {
                System.out.println("point " + point.x + " " + point.y);
                    JSONArray keyPointArray = new JSONArray();
                    keyPointArray.put(point.getX());
                    keyPointArray.put(point.getY());
                System.out.println("keyPointArray " + keyPointArray);
                keyPoints.put(keyPointArray);
                System.out.println("keyPoints " + keyPoints);
            }
            levelProperties.put("keys", keyPoints);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        System.out.println("====================================");
        System.out.println("* Write field settings to JSONObject");
        Field[] settingsFields = settings.getClass().getDeclaredFields();
        System.out.println("  - Settings fields size " + settingsFields.length);
        for (Field field: settingsFields) {
            field.setAccessible(true);
            try {
                System.out.println("    # " + field.getName() + " " + field.getType() + " " + field.get(settings));
                fieldProperties.put(field.getName(), field.get(settings));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        System.out.println("* Write field settings to resulting JSONObject");
        try {
            propertiesJSON.put(FIELD_PROPERTIES, fieldProperties);
            propertiesJSON.put(LEVEL_PROPERTIES, levelProperties);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("* Write platforms to JSONArray");
        for (Platform platform : platforms) {
            JSONObject jsonObject = new JSONObject();
            Field[] platformFields = platform.getClass().getDeclaredFields();
            for (Field field: platformFields) {
                if (field.getName().equals("indexGraphic"))
                    continue;
                if (field.getName().equals("points")) {
                    field.setAccessible(true);
                    try {
                        JSONArray array = new JSONArray();
                        for (Point point : (List<Point>) field.get(platform)) {
                            JSONObject coordinate = new JSONObject();
                            coordinate.put("x", point.x);
                            coordinate.put("y", point.y);
                            array.put(coordinate);
                        }
                        System.out.println("    # " + field.getName() + " " + field.getType() + " " + array);
                        jsonObject.put(field.getName(), array);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        return false;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                if (field.getName().equals("properties")) {
                    field.setAccessible(true);
                    try {
                        JSONObject platformProperties = new JSONObject((HashMap<String, String>) field.get(platform));
                        jsonObject.put("properties", platformProperties);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        return false;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            }
            platformsProperties.put(jsonObject);
        }
        System.out.println("* Write platforms to resulting JSONObject");
        try {
            propertiesJSON.put(PLATFORM_PROPERTIES, platformsProperties);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("* Result");
        System.out.println(propertiesJSON.toString());

        System.out.println("* Writing json to file " + fileToSave);
        boolean isSuccess = writeToFile(fileToSave, propertiesJSON.toString());
        System.out.println("* is success " + isSuccess + " " + fileToSave);
        return isSuccess;
    }

    private boolean writeToFile(String filePath, String jsonString) {
        try {
            System.out.println("file " + filePath);
            PrintWriter writer = new PrintWriter(filePath, "UTF-8");
            writer.println(jsonString);
            writer.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return  false;
    }

    public boolean open(Properties properties, String path) {
        System.out.println("====================================");
        System.out.println("* Read json from file " + path + ":");

        System.out.println("* Read field settings from JSONObject");
        JSONObject fieldProperties = null;

        JSONObject propertiesJSON = readFromFile(path);
        System.out.println("* " + propertiesJSON);
        try {

            fieldProperties = propertiesJSON.getJSONObject(FIELD_PROPERTIES);
            System.out.println("* " + fieldProperties.toString());

            FieldSettings settings = properties.getFieldSettings();
            Field[] settingsFields = settings.getClass().getDeclaredFields();
            System.out.println("  - Settings fields size " + settingsFields.length);
            for (Field field: settingsFields) {
                field.setAccessible(true);
                System.out.println("    # " + field.getName() + " " + field.getType() + " " + field.get(settings));
                if (field.getType().getName().equals("boolean")) {
                    field.setBoolean(settings, (boolean) fieldProperties.opt(field.getName()));
                } else if (field.getType().getName().equals("int")) {
                    field.setInt(settings, (int) fieldProperties.opt(field.getName()));
                } else  {
                    field.set(settings, fieldProperties.opt(field.getName()));
                }
                System.out.println("    # " + field.getName() + " " + field.getType() + " " + field.get(settings));
            }

            HashMap<String, String> levelProperties = new HashMap<>();
            JSONObject object = propertiesJSON.getJSONObject(LEVEL_PROPERTIES);
            Iterator<String> keys = object.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("keys")) {
                    ArrayList<Point> keysList = new ArrayList<Point>();
                    JSONArray keyPointsArray = object.getJSONArray("keys");
                    for (int i =0; i < keyPointsArray.length(); i++) {
                        Point keyPoint = new Point();
                        JSONArray keyPointPointArray = (JSONArray) keyPointsArray.get(i);//bject.getJSONArray("key");
                        keyPoint.x = keyPointPointArray.getInt(0) / settings.getScaleFactor();
                        keyPoint.y = keyPointPointArray.getInt(1) / settings.getScaleFactor();
                        keysList.add(keyPoint);
                    }
                    properties.setKeyPoints(keysList);
                } else
                    levelProperties.put(key, object.optString(key));
            }
            properties.setLevelSettings(levelProperties);




        JSONArray platformsProperties = propertiesJSON.getJSONArray(PLATFORM_PROPERTIES);
        for (int i = 0; i < platformsProperties.length(); i++) {
            JSONObject obj = platformsProperties.getJSONObject(i);
            Platform platform = new Platform();
            Field[] platformFields = platform.getClass().getDeclaredFields();
            for (Field field: platformFields) {
                if (field.getName().equals("indexGraphic"))
                    continue;
                if (field.getName().equals("points")) {
                    field.setAccessible(true);
                    List<Point> points = new ArrayList<Point>();
                    try {
                        JSONArray array = obj.getJSONArray(field.getName());
                        for (int j = 0; j < array.length(); j++) {
                            JSONObject coordinate = array.getJSONObject(j);
                            Point point = new Point(coordinate.getInt("x") / settings.getScaleFactor(), coordinate.getInt("y") / settings.getScaleFactor());
                            platform.addPoint(point, -1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            }


            HashMap<String, String> platformProperties = new HashMap<>();
            object = obj.getJSONObject("properties");
            keys = object.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                platformProperties.put(key, object.optString(key));
            }
            platform.setProperties(platformProperties);

            properties.getPlatformList().add(platform);
        }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        } catch (ClassCastException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private JSONObject readFromFile(String path) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(path));
            String line = null;
            String result = "";
            while ((line = reader.readLine()) != null) {
                result += line;
            }
            JSONObject object = new JSONObject(result);
            return  object;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONArray readFromFileToArray(String path) {
        BufferedReader reader = null;
        try {
            File file = new File(path);
            if (!file.exists())
                return  null;

            reader = new BufferedReader(new FileReader(path));
            String line = null;
            String result = "";
            while ((line = reader.readLine()) != null) {
                result += line;
            }
            JSONArray object = new JSONArray(result);
            return  object;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean savePresets(String properties, boolean isLevelProperties) {
        if (properties == null || properties.isEmpty())
            return false;
        String[] propertiesList = properties.split("\\n");
        JSONArray array = new JSONArray();
        for (int i = 0; i < propertiesList.length; i++) {
            String[] splitProperties = propertiesList[i].split(divider);
            array.put(splitProperties[0].trim());
        }
        System.out.println(array.toString());
        String path = getPathToPresets(isLevelProperties);
        System.out.println(path);
        writeToFile(path, array.toString());

        return true;
    }

    private String getPathToPresets(boolean isLevelProperties) {
        String path = System.getProperty("user.dir");

        path += "\\presets";
        File file = new File(path);
        file.mkdir();
        if (isLevelProperties)
            path += "\\levelPresets.prts";
        else
            path += "\\platformPresets.prts";
        return path;
    }

    public HashMap<String, String> getPropertiesMap(String properties) throws Exception {
        if (properties == null || properties.isEmpty()) {
            throw new Exception("Error in parameters!");
        }
        System.out.println("After exception");

        HashMap<String, String> propertiesMap = new HashMap<>();
        String[] propertiesList = properties.split("\\n");

        for (int i = 0; i < propertiesList.length; i++) {
            String[] splitProperties = propertiesList[i].split(divider);
            if (splitProperties.length < 2)
                throw new Exception("Error in parameters!");
            propertiesMap.put(splitProperties[0].trim(), splitProperties[1].trim());
        }
        return propertiesMap;
    }


    public HashMap<String, String> getPresets(boolean isLevelPresets) {
        HashMap<String, String> platformPresets = new HashMap<>();
        String pathToPresets = getPathToPresets(isLevelPresets);
        JSONArray array = readFromFileToArray(pathToPresets);
        if (array == null)
            return new HashMap<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                platformPresets.put((String) array.get(i), "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return platformPresets;
    }
}
