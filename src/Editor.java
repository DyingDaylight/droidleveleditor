import listener.PlatformListListener;
import listener.PropertiesButtonListener;
import listener.SettingsDialogListener;
import listener.SurfaceEventsListener;
import manager.SaveManager;
import model.FieldSettings;
import model.Platform;
import model.Properties;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.HashMap;

/**
 * Created by KaterinaG on 04.08.2014.
 * Main Class
 */
public class Editor extends JFrame implements SurfaceEventsListener {

    PropertiesWidget propertiesWidget;
    PlatformsWidget platformsWidget;
    JToolBar propertiesToolBar;
   // JToolBar platformsToolBar;
    JScrollPane scrollPane;
    JLabel statusBarPoint;
    JLabel statusBarZoom;
    JLabel statusBarTitle;
    JCheckBoxMenuItem propertiesSettings;
    JCheckBoxMenuItem platformsList;

    Surface surface;

    Properties properties = new Properties();

   int selectedPlatformIndex = -1;

    SaveManager saveManager;

    String saveFilePath = "";
    String levelName = "Level1";
    String extension = "prts";

    public Editor() {
        initUI();
    }

    private void initUI() {
        initMenuBar();

        initToolPanel();

        setStatusBar();
        saveManager = new SaveManager();
        surface = new Surface(this, properties);

        JPanel surfaceContainer = new JPanel(new FlowLayout(FlowLayout.LEFT));
        surfaceContainer.add(surface);

        scrollPane = new JScrollPane(surfaceContainer);
        scrollPane.setWheelScrollingEnabled(true);
        scrollPane.getVerticalScrollBar().addAdjustmentListener(adjustmentListener);
        scrollPane.getHorizontalScrollBar().addAdjustmentListener(adjustmentListener);
        add(scrollPane);

        scrollPane.revalidate();

        initPropertiesBar();
        initPlatformsWidget();

        pack();

        setTitle("Platform Editor");
        setSize(1000, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    private void initPropertiesBar() {
        propertiesWidget = new PropertiesWidget(propertiesButtonListener);
        propertiesWidget.setPreferredSize(new Dimension(300, 300));
        propertiesWidget.setVisible(false);
        propertiesToolBar = new JToolBar();
        propertiesToolBar.add(propertiesWidget);
        add(propertiesToolBar, BorderLayout.EAST);
        propertiesToolBar.setVisible(true);
    }

    private void initPlatformsWidget() {
        platformsWidget = new PlatformsWidget(platformListListener, properties.getPlatformList());
        platformsWidget.setVisible(false);
       // platformsToolBar = new JToolBar();
        propertiesToolBar.add(platformsWidget);
        //add(platformsToolBar, BorderLayout.EAST);
      //  platformsToolBar.setVisible(false);
    }

    private void setStatusBar() {
        JPanel statusBarPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        statusBarPoint = new JLabel(" Point:");
        statusBarZoom = new JLabel(" Zoom: ");
        statusBarTitle = new JLabel(" " + levelName + " ");
        statusBarPanel.add(statusBarTitle);
        statusBarPanel.add(statusBarPoint);
        statusBarPanel.add(statusBarZoom);
        statusBarPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        add(statusBarPanel, BorderLayout.SOUTH);
    }

    private void initToolPanel() {

        JToolBar toolBar = new JToolBar();

        ImageIcon drawIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/pencil_icon.png"));
        ImageIcon selectIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/select_icon.png"));
        ImageIcon moveIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/move_icon.png"));
        ImageIcon deleteIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/delete_icon.png"));
        ImageIcon zoomInIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/zoom_icon.png"));
        ImageIcon clearIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/clear_icon_big.png"));
        ImageIcon startIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/start_icon.png"));

        JToggleButton drawButton = new JToggleButton(drawIcon);
        JToggleButton selectButton = new JToggleButton(selectIcon);
        JToggleButton moveButton = new JToggleButton(moveIcon);
        JToggleButton deleteButton = new JToggleButton(deleteIcon);
        JToggleButton startPointButton = new JToggleButton(startIcon);
        JButton zoomInButton = new JButton(zoomInIcon);
        JButton zoomOutButton = new JButton(zoomInIcon);
        JButton clearButton = new JButton(clearIcon);

        drawButton.setSelected(true);

        toolBar.add(drawButton);
        toolBar.add(selectButton);
        toolBar.add(moveButton);
        toolBar.add(deleteButton);
        toolBar.add(startPointButton);
        toolBar.addSeparator();
        toolBar.add(zoomInButton);
        toolBar.add(zoomOutButton);
        toolBar.addSeparator();
        toolBar.add(clearButton);

        drawButton.setToolTipText("Draw a point");
        drawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                surface.setCurrentMode(Surface.DRAW_MODE);
            }
        });

        selectButton.setToolTipText("Select a point");
        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                surface.setCurrentMode(Surface.SELECT_MODE);
            }
        });

        moveButton.setToolTipText("Move a point");
        moveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                surface.setCurrentMode(Surface.MOVE_MODE);
            }
        });

        deleteButton.setToolTipText("Delete a point");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                surface.setCurrentMode(Surface.DELETE_MODE);

            }
        });

        startPointButton.setToolTipText("Key point");
        startPointButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                surface.setCurrentMode(Surface.START_POINT_MODE);
            }
        });

        zoomInButton.setToolTipText("Zoom in");
        zoomInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                surface.zoomIn();
                scrollPane.revalidate();
            }
        });

        zoomOutButton.setToolTipText("Zoom out");
        zoomOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                surface.zoomOut();
                scrollPane.revalidate();
            }
        });

        clearButton.setToolTipText("Clear field");
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                properties.clearKeyPoints();
                surface.clear(properties.getKeyPoints());
                propertiesWidget.showLevelProperties(properties.getLevelSettings());
            }
        });

        ButtonGroup buttonGroup  = new ButtonGroup();
        buttonGroup.add(drawButton);
        buttonGroup.add(selectButton);
        buttonGroup.add(moveButton);
        buttonGroup.add(deleteButton);
        buttonGroup.add(startPointButton);

        add(toolBar, BorderLayout.NORTH);

    }

    private void initMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        JMenu menu = new JMenu("Settings");

        ImageIcon newIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/new_icon.png"));
        ImageIcon openIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/open_icon.png"));
        ImageIcon saveIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/save_icon.png"));
        ImageIcon clearIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/clear_icon.png"));
        ImageIcon exitIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/close_icon.png"));
        ImageIcon fieldSettingsIcon = new ImageIcon(this.getClass().getClassLoader().getResource("icons/field_settings_icon.png"));

        JMenuItem newItem = new JMenuItem("New");
        JMenuItem openItem = new JMenuItem("Open");
        JMenuItem saveItem = new JMenuItem("Save");
        JMenuItem saveAsItem = new JMenuItem("Save as...");
        JMenuItem clearItem = new JMenuItem("Clear");
        JMenuItem exit = new JMenuItem("Exit");
        JMenuItem fieldsSettings = new JMenuItem("Field Settings");
        propertiesSettings = new JCheckBoxMenuItem("Show settings panel");
        platformsList = new JCheckBoxMenuItem("Show platform list");


        newItem.setIcon(newIcon);
        newItem.setToolTipText("New level");
        newItem.setMnemonic(KeyEvent.VK_N);
        newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createNewLevel();
            }
        });

        openItem.setIcon(openIcon);
        openItem.setToolTipText("Open level");
        openItem.setMnemonic(KeyEvent.VK_O);
        openItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openLevel();
            }
        });

        saveItem.setIcon(saveIcon);
        saveItem.setToolTipText("Save Level");
        saveItem.setMnemonic(KeyEvent.VK_S);
        saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveLevel();
            }
        });

        saveAsItem.setToolTipText("Save Level as...");
        saveAsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveLevelAs();
            }
        });

        exit.setIcon(exitIcon);
        exit.setToolTipText("Exit application");
        exit.setMnemonic(KeyEvent.VK_E);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        clearItem.setIcon(clearIcon);
        exit.setToolTipText("Clear field");
        exit.setMnemonic(KeyEvent.VK_D);
        clearItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                properties.clearKeyPoints();
                surface.clear(properties.getKeyPoints());
                propertiesWidget.showLevelProperties(properties.getLevelSettings());
            }
        });

        fieldsSettings.setIcon(fieldSettingsIcon);
        fieldsSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SettingsDialog dialog = new SettingsDialog(onSettingsApproved, properties.getFieldSettings());
                dialog.setVisible(true);
            }
        });

        propertiesSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBoxMenuItem source = (JCheckBoxMenuItem) e.getSource();
                boolean state = source.isSelected();
                if (state) {
                    platformsList.setState(false);
                    platformsWidget.setVisible(false);
                }
                System.out.println("propertiesSettings action " + state);
                propertiesWidget.setVisible(state);
                propertiesWidget.showLevelProperties(properties.getLevelSettings());
                selectedPlatformIndex = -1;
            }
        });

        platformsList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBoxMenuItem source = (JCheckBoxMenuItem) e.getSource();
                boolean state = source.isSelected();
                if (state) {
                    propertiesSettings.setState(false);
                    propertiesWidget.setVisible(false);
                }
                platformsWidget.setVisible(state);
            }
        });

        file.add(newItem);
        file.add(openItem);
        file.add(saveItem);
        file.add(saveAsItem);
        file.addSeparator();
        file.add(clearItem);
        file.addSeparator();
        file.add(exit);

        menu.add(fieldsSettings);
        menu.add(propertiesSettings);
        menu.add(platformsList);

        menuBar.add(file);
        menuBar.add(menu);
        setJMenuBar(menuBar);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Editor editor = new Editor();
                editor.setVisible(true);
            }
        });
    }

    @Override
    public void onLeftMouseReleased(Point point) {
        String positionStr = " Point: x: " + point.x + ", y: " + point.y + " |";
        statusBarPoint.setText(positionStr);
    }

    @Override
    public void onZoomChanged(double scale) {
        if (statusBarZoom == null)
            return;
        String zoomStr = " Zoom: " + String.valueOf(Math.round(scale * 100) + "%");
        statusBarZoom.setText(zoomStr);
    }

    @Override
    public void onPlatformSelected(int platformIndex) {
        showPlatformProperties(platformIndex);
    }

    private void showPlatformProperties(int platformIndex) {
        selectedPlatformIndex = platformIndex;
        if (!propertiesSettings.isSelected()) {
            propertiesWidget.setVisible(true);
            propertiesSettings.setSelected(true);
        }
        if (platformIndex == -1)
            propertiesWidget.showLevelProperties(properties.getLevelSettings());
        else
            propertiesWidget.showPlatformProperties(properties.getPlatformList().get(platformIndex), platformIndex);
    }

    @Override
    public void onPlatformDeleted(int platformIndex) {
        if (!propertiesWidget.getIsLevelSettings() && selectedPlatformIndex == platformIndex) {
            propertiesWidget.setVisible(false);
            propertiesSettings.setSelected(false);
            selectedPlatformIndex = -1;
        }
    }

    @Override
    public void onPlatformsChanged() {
        if (platformsWidget != null)
            platformsWidget.updatePlatforms(properties.getPlatformList());
    }

    SettingsDialogListener onSettingsApproved = new SettingsDialogListener() {
        @Override
        public void onSettingsGot(FieldSettings settings) {
            properties.setFieldSettings(settings);
            setFieldSettings();
        }
    };

    AdjustmentListener adjustmentListener = new AdjustmentListener() {
        @Override
        public void adjustmentValueChanged(AdjustmentEvent evt) {
            if (evt.getValueIsAdjusting()) {
                return;
            }

            Adjustable source = evt.getAdjustable();
            int orient = source.getOrientation();
            int value = evt.getValue();

            Rectangle rect = new Rectangle();
            surface.getBounds(rect);

            if (orient == Adjustable.HORIZONTAL) {
                value = Math.max(0, value - rect.x);
                surface.setHorizontalOffset(value);
            } else {
                value = Math.max(0, value - rect.y);
                surface.setVerticalOffset(value);
            }
        }
    };

    PropertiesButtonListener propertiesButtonListener = new PropertiesButtonListener() {
        @Override
        public void onClose() {
            propertiesWidget.setVisible(false);
            propertiesSettings.setSelected(false);
        }

        @Override
        public void onSavePresets(boolean isLevelProperties) {
            boolean isSuccess = saveManager.savePresets(propertiesWidget.getProperties(), isLevelProperties);
            if (!isSuccess) {
                JOptionPane.showMessageDialog(Editor.this, "Failed to save presets!",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        @Override
        public void onSaveProperties() {
            try {
                HashMap<String, String> propertiesMap = saveManager.getPropertiesMap(propertiesWidget.getProperties());
                if (propertiesWidget.getIsLevelSettings()) {
                    properties.setLevelSettings(propertiesMap);
                } else {
                    Platform platform = properties.getPlatformList().get(selectedPlatformIndex);
                    if (platform != null)
                        platform.setProperties(propertiesMap);

                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(Editor.this, "Can't save settings!",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    };

    private void createNewLevel() {
        levelName = (String) JOptionPane.showInputDialog(this,
                "Enter new level name:",
                "New level name",
                JOptionPane.PLAIN_MESSAGE);

        if (levelName == null)
            return;

        if (levelName.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Name can't be empty",
                    "Error", JOptionPane.ERROR_MESSAGE);
            createNewLevel();
            return;
        }

        statusBarTitle.setText(levelName);

        resetFieldProperties();
        resetLevelProperties();
        properties.clearKeyPoints();
        surface.clear(properties.getKeyPoints());
        propertiesWidget.showLevelProperties(properties.getLevelSettings());
    }

    private void saveLevel() {
        if (saveFilePath == null || saveFilePath.isEmpty()) {
            JFileChooser chooseDirectory = new JFileChooser();
            chooseDirectory.setDialogTitle("Choose Directory");
            chooseDirectory.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooseDirectory.setAcceptAllFileFilterUsed(false);

            int ret = chooseDirectory.showDialog(this, "Save");

            if (ret == JFileChooser.APPROVE_OPTION) {
                saveFilePath = chooseDirectory.getSelectedFile() + "\\" + levelName + "." + extension;
                System.out.println("saveFilePath " +  saveFilePath);
            } else {
                return;
            }
        }
        save(saveFilePath);
    }

    private void saveLevelAs() {
        JFileChooser chooseDirectory = new JFileChooser();
        chooseDirectory.setDialogTitle("Choose file");
        chooseDirectory.setCurrentDirectory(new File(saveFilePath));
        FileFilter filter = new FileNameExtensionFilter(extension + " files", extension);
        chooseDirectory.setFileFilter(filter);
        chooseDirectory.setAcceptAllFileFilterUsed(false);

        int ret = chooseDirectory.showDialog(this, "Save as");

        if (ret == JFileChooser.APPROVE_OPTION) {
            String path = chooseDirectory.getSelectedFile().toString();
            if (! path.endsWith("." + extension)) {
                path += "." + extension;
            }
            System.out.println("saveFilePath " +  path);
            save(path);
        }
    }

    private void save(String path) {
        Properties copiedProperties = properties.copy();
        java.util.List<Platform> platforms = copiedProperties.getPlatformList();
        if (platforms == null || platforms.isEmpty()) {
            JOptionPane.showMessageDialog(this, "No platforms to save!",
                    "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        for (Platform platform : platforms) {
            if (!platform.isPlatformFinished()) {
                JOptionPane.showMessageDialog(this, "You have to finish all platforms first! Platform "
                                + platforms.indexOf(platform) + " is not finished!",
                        "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        boolean isSuccess = saveManager.save(copiedProperties, path);
        if (!isSuccess) {
            JOptionPane.showMessageDialog(this, "Failed to save the level!",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void openLevel() {
        JFileChooser chooseDirectory = new JFileChooser();
        chooseDirectory.setDialogTitle("Open file");
        chooseDirectory.setCurrentDirectory(new File(saveFilePath));
        FileFilter filter = new FileNameExtensionFilter(extension + " files", extension);
        chooseDirectory.setFileFilter(filter);
        chooseDirectory.setAcceptAllFileFilterUsed(false);

        int ret = chooseDirectory.showDialog(this, "Open");

        if (ret == JFileChooser.APPROVE_OPTION) {
            levelName = chooseDirectory.getSelectedFile().getName();
            saveFilePath = chooseDirectory.getSelectedFile().toString();
            System.out.println("openFilePath " + saveFilePath);
            resetFieldProperties();
            resetLevelProperties();
            surface.clear(properties.getKeyPoints());
            propertiesWidget.showLevelProperties(properties.getLevelSettings());

            SaveManager sm = new SaveManager();
            boolean isSuccess = sm.open(properties, saveFilePath);
            if (!isSuccess) {
                JOptionPane.showMessageDialog(this, "Failed to open file!",
                        "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            statusBarTitle.setText(levelName);
            setFieldSettings();
            System.out.println("s " + properties.getPlatformList().size());
            platformsWidget.updatePlatforms(properties.getPlatformList());
        }
    }

    private void resetLevelProperties() {
    }

    private void resetFieldProperties() {
        properties = new Properties();
        setFieldSettings();
    }

    private void setFieldSettings() {
        surface.resetFieldSettings(properties);
    }

    private PlatformListListener platformListListener = new PlatformListListener() {
        @Override
        public void onClose() {
            platformsWidget.setVisible(false);
            platformsList.setState(false);
        }

        @Override
        public void onDoubleClick(int index) {
            showPlatformProperties(index);
        }
    };
}
