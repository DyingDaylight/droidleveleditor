import listener.PlatformListListener;
import model.Platform;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by kettricken on 18.09.2014.
 */
public class PlatformsWidget extends JPanel {

    JLabel title;
    JList list;

    PlatformListListener platformListListener;
    java.util.ArrayList<Platform> platformList;

    public PlatformsWidget(PlatformListListener platformListListener, java.util.ArrayList<Platform> platformList) {
        this.platformListListener = platformListListener;
        this.platformList = platformList;
        initUI();
    }

    private void initUI() {

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        title = new JLabel("PlatformsList");
        add(title, BorderLayout.NORTH);

        JScrollPane pane = new JScrollPane();

        Platform[] data = platformList.toArray(new Platform[platformList.size()]);
        list = new JList(data);
        list.setCellRenderer(new PlatformCellRenderer());

        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);

        list.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList list = (JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    int index = list.locationToIndex(evt.getPoint());
                    platformListListener.onDoubleClick(index);
                }
            }
        });

        pane.getViewport().add(list);

        add(pane);

        JPanel buttons = new JPanel();

        JButton deleteBtn = new JButton("Delete");
        JButton closeBtn = new JButton("Close");

        deleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selected = list.getSelectedIndex();
                System.out.println("selected " + selected);
                if (selected < 0)
                    return;
                platformList.remove(selected);
                updatePlatforms(platformList);
            }
        });

        closeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                platformListListener.onClose();
            }
        });

        buttons.add(deleteBtn);
        buttons.add(closeBtn);

        add(buttons, BorderLayout.SOUTH);

    }

    public void updatePlatforms(ArrayList<Platform> platformList) {
        System.out.println("update platforms");
        this.platformList = platformList;
        Platform[] data = platformList.toArray(new Platform[platformList.size()]);
        System.out.println("size " + data.length);
        list.setListData(data);
        list.repaint();
    }

    class PlatformCellRenderer extends JLabel implements ListCellRenderer {

        private final Color HIGHLIGHT_COLOR = new Color(0, 0, 128);

        public PlatformCellRenderer() {
            setOpaque(true);
        }

        public Component getListCellRendererComponent(JList list, Object value,
                                                      int index, boolean isSelected, boolean cellHasFocus) {
            Platform platform = (Platform) value;
            setText("Platform " + platformList.indexOf(platform));

            if (isSelected) {
                setBackground(HIGHLIGHT_COLOR);
                setForeground(Color.white);
            } else {
                setBackground(Color.white);
                setForeground(Color.black);
            }
            return this;
        }


    }

}
