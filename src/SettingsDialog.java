import listener.SettingsDialogListener;
import model.FieldSettings;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by KaterinaG on 05.08.2014.
 * Dialog for settings.
 * Sets field size.
 */
public class SettingsDialog extends JDialog {

    public static final int MIN_WIDTH = 1920;
    public static final int MIN_HEIGHT = 1080;
    private final int MIN_ROUND_TO = 5;
    private final int MIN_SCALE_FACTOR = 2;
    private final int inputFieldWidth = 60;
    private final int inputFieldHeight = 25;

    SettingsDialogListener listener;

    FieldSettings settings;

    JTextField heightTextField;
    JTextField widthTextField;
    JTextField roundToTextField;
    JTextField scaleTextField;
    JLabel errorLabel;
    JLabel directionMessage;

    public SettingsDialog(SettingsDialogListener listener, FieldSettings settings) {
        this.listener = listener;
        this.settings = FieldSettings.copy(settings);
        initUI();
    }

    private void initUI() {
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        add(Box.createRigidArea(new Dimension(0, 10)));

        JLabel fieldSizeTitle = new JLabel("Field size");
        fieldSizeTitle.setAlignmentX(0.5f);
        add(fieldSizeTitle);

        add(Box.createRigidArea(new Dimension(0, 5)));

        initSizePanel();

        add(Box.createRigidArea(new Dimension(0, 5)));

        initRoundToPanel();

        add(Box.createRigidArea(new Dimension(0, 5)));

        initScaleFactorPanel();

        add(Box.createRigidArea(new Dimension(0, 5)));

        initCheckBoxSettings();

        initBottomPanel();

        add(Box.createRigidArea(new Dimension(0, 15)));

        pack();

        setTitle("Settings");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setLocationRelativeTo(null);
        setSize(new Dimension(500,400));
        setResizable(false);
        countRatio();
    }

    private void initCheckBoxSettings() {
        JCheckBox showVertexesIndex = new JCheckBox("Show vertexes' index", settings.showVertexesIndex());
        showVertexesIndex.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBox source = (JCheckBox) e.getSource();
                boolean state = source.isSelected();
                settings.setShowVertexesIndex(state);
            }
        });
        add(showVertexesIndex);

        JCheckBox showPlatformIndex = new JCheckBox("Show platform's indexes", settings.isShowPlatformIndex());
        showPlatformIndex.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JCheckBox source = (JCheckBox) e.getSource();
                boolean state = source.isSelected();
                settings.setShowPlatformIndex(state);
            }
        });
        add(showPlatformIndex);
    }

    private void initRoundToPanel() {
        JLabel label = new JLabel("Round point to: ");
        roundToTextField = new JTextField(String.valueOf(settings.getRoundTo()));
        roundToTextField.setPreferredSize(new Dimension(inputFieldWidth, inputFieldHeight));
        roundToTextField.setHorizontalAlignment(JTextField.RIGHT);
        roundToTextField.setBorder(BorderFactory.createCompoundBorder(
                roundToTextField.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        JPanel roundToPanel = new JPanel();
        roundToPanel.add(label);
        roundToPanel.add(roundToTextField);
        add(roundToPanel);
    }

    private void initScaleFactorPanel() {
        JLabel label = new JLabel("Scale factor: ");
        scaleTextField = new JTextField(String.valueOf(settings.getScaleFactor()));
        scaleTextField.setPreferredSize(new Dimension(inputFieldWidth, inputFieldHeight));
        scaleTextField.setHorizontalAlignment(JTextField.RIGHT);
        scaleTextField.setBorder(BorderFactory.createCompoundBorder(
                scaleTextField.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        JPanel roundToPanel = new JPanel();
        roundToPanel.add(label);
        roundToPanel.add(scaleTextField);
        add(roundToPanel);
    }

    private void initBottomPanel() {
        JPanel bottom = new JPanel();
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.Y_AXIS));

        errorLabel = new JLabel("");
        errorLabel.setForeground(Color.red);
        errorLabel.setAlignmentX(0.5f);

        JPanel buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));

        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (validateSettings()) {
                    listener.onSettingsGot(settings);
                    dispose();
                }
            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        buttons.add(okButton);
        buttons.add(Box.createRigidArea(new Dimension(30, 0)));
        buttons.add(cancelButton);

        bottom.add(errorLabel);
        buttons.add(Box.createRigidArea(new Dimension(0, 30)));
        bottom.add(buttons);
        add(bottom);
    }

    private void initSizePanel() {
        JPanel sizePanel = new JPanel();

        JLabel widthLabel = new JLabel("Width: ");
        JLabel xLabel = new JLabel(" * ");
        JLabel heightLabel = new JLabel("Height: ");

        widthTextField = new JTextField(String.valueOf(settings.getFieldWidth()));
        widthTextField.setPreferredSize(new Dimension(inputFieldWidth, inputFieldHeight));
        widthTextField.setHorizontalAlignment(JTextField.RIGHT);
        widthTextField.setBorder(BorderFactory.createCompoundBorder(
                widthTextField.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        heightTextField = new JTextField(String.valueOf(settings.getFieldHeight()));
        heightTextField.setPreferredSize(new Dimension(inputFieldWidth, inputFieldHeight));
        heightTextField.setHorizontalAlignment(JTextField.RIGHT);
        heightTextField.setBorder(BorderFactory.createCompoundBorder(
                heightTextField.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        sizePanel.add(widthLabel);
        sizePanel.add(widthTextField);
        sizePanel.add(xLabel);
        sizePanel.add(heightLabel);
        sizePanel.add(heightTextField);

        add(Box.createRigidArea(new Dimension(0, 0)));

        directionMessage = new JLabel("Direction message");


        widthTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                System.out.println("changeUpdate");
            }

            public void removeUpdate(DocumentEvent e) {
                System.out.println("removeUpdate");
            }

            public void insertUpdate(DocumentEvent e) {
                System.out.println("insertUpdate");
                countRatio();
            }

            public void warn() {
                if (Integer.parseInt(widthTextField.getText()) <= 0) {
                    JOptionPane.showMessageDialog(null,
                            "Error: Please enter number bigger than 0", "Error Massage",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        add(sizePanel);
        add(directionMessage);


    }

    public void countRatio(){
        try {
            int width = Integer.valueOf(widthTextField.getText());
            int height = Integer.valueOf(heightTextField.getText());
            if (width < MIN_WIDTH || height < MIN_HEIGHT) {
                errorLabel.setText("Fields size is too small");
                return;
            }
            float zoomByWidth =width / (float) MIN_WIDTH;
            float zoomByHeight = height / (float) MIN_HEIGHT;
            if (zoomByHeight > zoomByWidth)
                directionMessage.setText("vertical run");
            else
                directionMessage.setText("horizontal run");

        }catch (NumberFormatException e) {
            errorLabel.setText("Wrong numbers for field size");
        }
    }

    private boolean validateSettings() {
        try {
            int width = Integer.valueOf(widthTextField.getText());
            int height = Integer.valueOf(heightTextField.getText());
            if (width < MIN_WIDTH || height < MIN_HEIGHT) {
                errorLabel.setText("Fields size is too small");
                return false;
            }
            settings.setFieldHeight(height);
            settings.setFieldWidth(width);
        }catch (NumberFormatException e) {
            errorLabel.setText("Wrong numbers for field size");
            return false;
        }

        try {
            int roundTo = Integer.valueOf(roundToTextField.getText());
            if (roundTo < MIN_ROUND_TO) {
                errorLabel.setText("RoundTo value is too small");
                return false;
            }
            settings.setRoundTo(roundTo);
        }catch (NumberFormatException e) {
            errorLabel.setText("Wrong numbers for RoundToValue");
            return false;
        }

        try {
            int scale = Integer.valueOf(scaleTextField.getText());
            if (scale < MIN_SCALE_FACTOR) {
                errorLabel.setText("Scale value is too small");
                return false;
            }
            settings.setScaleFactor(scale);
        }catch (NumberFormatException e) {
            errorLabel.setText("Wrong numbers for Scale factor");
            return false;
        }
        return true;
    }

}
