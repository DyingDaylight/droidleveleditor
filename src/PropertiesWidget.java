import listener.PropertiesButtonListener;
import model.Platform;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kettricken on 06.08.2014.
 */
public class PropertiesWidget extends JPanel {

    PropertiesButtonListener listener;
    JLabel title;
    JTextArea propertiesInput;

    boolean levelSettings = true;

    public PropertiesWidget(PropertiesButtonListener listener) {
        initUI(listener);
    }

    private void initUI(final PropertiesButtonListener listener) {
        this.listener = listener;

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        title = new JLabel("Level properties");
        add(title, BorderLayout.NORTH);

        JScrollPane pane = new JScrollPane();
        propertiesInput = new JTextArea();

        propertiesInput.setLineWrap(true);
        propertiesInput.setWrapStyleWord(true);
        propertiesInput.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));

        pane.getViewport().add(propertiesInput);

        add(pane);

        JPanel buttons = new JPanel();

        JButton saveProperties = new JButton("Save");
        JButton savePresets = new JButton("Save presets");
        JButton closeBtn = new JButton("Close");

        saveProperties.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (listener != null)
                    listener.onSaveProperties();
            }
        });

        savePresets.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (listener != null)
                    listener.onSavePresets(levelSettings);
            }
        });

        closeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (listener !=null)
                    listener.onClose();
            }
        });

        buttons.add(saveProperties);
        buttons.add(savePresets);
        buttons.add(closeBtn);

        add(buttons, BorderLayout.SOUTH);

    }


    public void showPlatformProperties(Platform platform, int index) {
        levelSettings = false;
        title.setText("Platform " + index + " properties");
        String properties = "";
        HashMap<String, String> propertiesMap = platform.getProperties();
        for (Map.Entry<String, String> entry : propertiesMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            properties += key + " = " + value + "\n";
        }

        propertiesInput.setText(properties);
    }

    public void showLevelProperties(HashMap<String, String> levelProperties) {
        levelSettings = true;
        title.setText("Level properties");

        String properties = "";
        if (levelProperties != null) {
            for (Map.Entry<String, String> entry : levelProperties.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                properties += key + " = " + value + "\n";
            }
        }

        propertiesInput.setText(properties);
    }

    public String getProperties() {
        return propertiesInput.getText();
    }

    public boolean getIsLevelSettings(){
        return levelSettings;
    }


}
