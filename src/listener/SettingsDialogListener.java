package listener;

import model.FieldSettings;

/**
 * Created by KaterinaG on 05.08.2014.
 * Listener for settings dialog.
 */
public interface SettingsDialogListener {

    public void onSettingsGot(FieldSettings settings);
}
