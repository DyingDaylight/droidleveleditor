package listener;

/**
 * Created by kettricken on 10.08.2014.
 */
public interface PropertiesButtonListener {
    public void onClose();
    public void onSavePresets(boolean isLevelProperties);
    public void onSaveProperties();
}
