package listener;

import java.awt.*;

/**
 * Created by KaterinaG on 05.08.2014.
 * Listener for surface events to show statistics in status bar.
 */
public interface SurfaceEventsListener {

    public void onLeftMouseReleased(Point point);
    public void onZoomChanged(double scale);
    public void onPlatformSelected(int platformIndex);
    public void onPlatformDeleted(int platformIndex);
    public void onPlatformsChanged();

}
