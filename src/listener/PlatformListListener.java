package listener;

/**
 * Created by kettricken on 18.09.2014.
 */
public interface PlatformListListener {

    public void onClose();

    void onDoubleClick(int index);
}
