import listener.SurfaceEventsListener;
import model.FieldSettings;
import model.Platform;
import model.Properties;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.NoninvertibleTransformException;
import java.util.ArrayList;

/**
 * Created by KaterinaG on 04.08.2014.
 * Surface to draw on.
 */
public class Surface extends JPanel implements MouseListener, MouseMotionListener {

    public static final int DRAW_MODE = 0;
    public static final int SELECT_MODE = 1;
    public static final int MOVE_MODE = 2;
    public static final int DELETE_MODE = 3;
    public static final int START_POINT_MODE = 4;
    public static final int MAX_ZOOM = 5;
    public static final double MIN_ZOOM = 0.5;
    public static final double ZOOM_IN_RATE = 0.2d;
    public static final double ZOOM_OUT_RATE = 0.1d;

    private boolean showVertexIndex = true;
    private boolean showPlatformIndexes = true;

    private int currentMode = 0;
    private int width = SettingsDialog.MIN_WIDTH;
    private int height = SettingsDialog.MIN_HEIGHT;
    private int roundTo = 10;
    private double scale = 1;
    private int scaleFactor =  2;

    private AffineTransform inverseScale;
    private AffineTransform transformScale;

    private final BasicStroke pointStroke;
    private final BasicStroke lineStroke;
    private final BasicStroke dashedLineStroke;

    private int horizontalOffset;
    private int verticalOffset;
    final private int textOffset = 10;

    SurfaceEventsListener listener;

    int currentPolygonIndex = -1;
    java.util.List<Platform> polygons;
    java.util.List<Ellipse2D> platformIndexes = new ArrayList<Ellipse2D>();

    Point selectedPoint;
    Point mousePosition;

   // Point startPoint;
    ArrayList<Point> keys = new ArrayList<>();

    public Surface(SurfaceEventsListener listener, Properties properties){
        this.listener = listener;

        addMouseListener(this);
        addMouseMotionListener(this);

        resetFieldSettings(properties);

        polygons = properties.getPlatformList();
        listener.onPlatformsChanged();
        keys = properties.getKeyPoints();

        setScale();
        setBackground(Color.BLACK);

        float[] dash = new float[] {6f,0f,0f};
        pointStroke = new BasicStroke(8, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
        lineStroke = new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL);
        dashedLineStroke = new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL, 1f, dash, 2f);
    }

    @Override
    public void paintComponent(Graphics graphics){
        super.paintComponent(graphics);
        doDrawing(graphics);
    }

    // --------- Mouse Events -------------------------

    @Override
    public void mouseDragged(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            switch (currentMode){
                case MOVE_MODE:
                    movePoint(e);
                    break;
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (currentMode == DRAW_MODE) {
            mousePosition = getRoundedPoint(e.getPoint());
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            switch (currentMode) {
                case MOVE_MODE:
                    Point point = getRoundedPoint(e.getPoint());
                    findPointInPolygon(point);
                    repaint();
                    break;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            switch (currentMode){
                case DRAW_MODE:
                    drawPoint(e);
                    break;
                case SELECT_MODE:
                    selectPoint(e);
                    break;
                case DELETE_MODE:
                    deletePoint(e);
                    break;
                case START_POINT_MODE:
                    setStartPoint(e);
                    break;
            }
        } else if (SwingUtilities.isRightMouseButton(e)) {
            deselectPoint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {   }

    @Override
    public void mouseExited(MouseEvent e) {   }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e) && currentMode == SELECT_MODE) {
            for (Platform p : polygons) {
                if (p.isPlatformFinished() && p.getIndexGraphic().contains(e.getX(), e.getY())) {
                    listener.onPlatformSelected(polygons.indexOf(p));
                    return;
                }
            }
            listener.onPlatformSelected(-1);
        }
    }

    // set parameters
    public void setCurrentMode(int mode){
        currentMode = mode;
        repaint();
    }

    public void setFieldSize(int width, int height, int scaleFactor) {
        this.width = width / scaleFactor;
        this.height = height / scaleFactor;
        setScale();
    }

    public void setHorizontalOffset(int horizontalOffset) {
//        Point offsets = new Point();
//        offsets.x = horizontalOffset;
//        offsets.y = verticalOffset;
//        inverseScale.transform(offsets, offsets);
//
//        this.horizontalOffset = offsets.x;
        repaint();
    }

    public void setVerticalOffset(int verticalOffset) {
//        Point offsets = new Point();
//        offsets.x = horizontalOffset;
//        offsets.y = verticalOffset;
//        inverseScale.transform(offsets, offsets);
//
//        this.verticalOffset = offsets.y;
        repaint();
    }

    public void setShowVertexIndex(boolean showVertexIndex) {
        this.showVertexIndex = showVertexIndex;
    }

    public void setShowPlatformIndexes(boolean showPlatformIndexes) {
        this.showPlatformIndexes = showPlatformIndexes;
    }

    public void setRoundTo(int roundTo) {
        this.roundTo = roundTo;
    }

    private void setScale() {
        listener.onZoomChanged(scale);
        transformScale = AffineTransform.getScaleInstance(scale, scale);
        try {
            inverseScale = transformScale.createInverse();
        } catch (NoninvertibleTransformException e) {
            // Ignoring the exception here. I know scaling can be inversed.
        }
        setPreferredSize(new Dimension((int) (width * scale), (int) (height * scale)));
        setBounds(getX(), getY(), (int) (width * scale), (int) (height * scale));
    }

    // Actions with a field

    public void zoomIn() {
        if (scale >= 1 && MAX_ZOOM - scale > ZOOM_IN_RATE) {
            scale += ZOOM_IN_RATE;
        } else if (scale < 1 && scale >= MIN_ZOOM) {
            scale += ZOOM_OUT_RATE;
        }
        setScale();
        invalidate();
        repaint();
    }

    public void zoomOut() {
        if (scale - MIN_ZOOM > ZOOM_OUT_RATE && scale <= 1) {
            scale -= ZOOM_OUT_RATE;
        } else if (scale > 1 && scale <= MAX_ZOOM) {
            scale -= ZOOM_IN_RATE;
        }
        setScale();
        invalidate();
        repaint();
    }

    public void clear(ArrayList<Point> keys){
        polygons.clear();
        listener.onPlatformsChanged();
        selectedPoint = null;
        currentPolygonIndex = -1;
        this.keys = keys;
        repaint();
    }

    // ----------------- Actions with points ------------------------
    private void movePoint(MouseEvent e) {
        Point point = getRoundedPoint(e.getPoint());
        if (point == null)
            return;
        if (selectedPoint == null)
            selectedPoint = new Point();
        selectedPoint.x = point.x;
        selectedPoint.y = point.y;
        repaint();
    }

    private void deletePoint(MouseEvent e) {
        Point point = getRoundedPoint(e.getPoint());
        findPointInPolygon(point);
        Platform polygon = polygons.get(currentPolygonIndex);
        polygon.deletePoint(selectedPoint);
        if (polygon.getAnglesNum() == 0) {
            if (listener != null)
                listener.onPlatformDeleted(currentPolygonIndex);
            polygons.remove(polygon);
            listener.onPlatformsChanged();
            currentPolygonIndex = -1;
        }
        deselectPoint();
        repaint();
    }

    private void selectPoint(MouseEvent e) {
        Point point = getRoundedPoint(e.getPoint());
        if (findPointInPolygon(point)) {
            repaint();
            return;
        }
        deselectPoint();
        repaint();
    }

    private void deselectPoint() {
        currentPolygonIndex = -1;
        selectedPoint = null;
    }

    private void drawPoint(MouseEvent e) {
        if (currentPolygonIndex == -1) {
            polygons.add(new Platform());
            currentPolygonIndex = polygons.size() - 1;
            listener.onPlatformsChanged();
        }

        Point point = getRoundedPoint(e.getPoint());

        Platform currentPolygon = polygons.get(currentPolygonIndex);
        if (currentPolygon.hasPoint(point) && !(currentPolygon.isStartPoint(point) && currentPolygon.getAnglesNum() > 1))
            return;

        int pointIndex = currentPolygon.getPointIndex(selectedPoint);
        currentPolygon.addPoint(point, pointIndex);

        selectedPoint = point;
        listener.onLeftMouseReleased(selectedPoint);

        if (currentPolygon.getAnglesNum() > 1 && currentPolygon.isStartPoint(point)) {
            deselectPoint();
        }
        repaint();
    }

    // ----------------- Drawing functions ---------------------------
    private void doDrawing(Graphics graphics) {
        Graphics2D g2d = (Graphics2D) graphics;

        AffineTransform transform = g2d.getTransform();
        transform.scale(transformScale.getScaleX(), transformScale.getScaleY());
        g2d.setTransform(transform);

        for (Platform polygon : polygons) {
            for (int i = 0; i < polygon.getAnglesNum(); i++) {
                g2d.setStroke(pointStroke);
                if (showVertexIndex) drawText(g2d, polygon.getPoint(i), String.valueOf(i), false, null);
                g2d.setColor(Color.blue);
                drawLine(g2d, polygon.getPoint(i), polygon.getPoint(i));
                drawLine(g2d, polygon.getPoint(i + 1), polygon.getPoint(i + 1));
                g2d.setStroke(lineStroke);
                drawLine(g2d, polygon.getPoint(i), polygon.getPoint(i + 1));
            }
            if (showPlatformIndexes && polygon.isPlatformFinished()) {
                Point point = polygon.getCenter();
                drawText(g2d, point, String.valueOf(polygons.indexOf(polygon)), true, polygon.getIndexGraphic());
            }
        }

        if (selectedPoint != null) {
            g2d.setColor(Color.red);
            g2d.setStroke(pointStroke);
            drawLine(g2d, selectedPoint, selectedPoint);
        }

        if (mousePosition != null && selectedPoint != null && currentMode == DRAW_MODE) {
            g2d.setColor(Color.blue);
            g2d.setStroke(dashedLineStroke);
            drawLine(g2d, selectedPoint, mousePosition);
        }

        for (Point key : keys) {
            g2d.setColor(Color.green);
            g2d.setStroke(pointStroke);
            drawLine(g2d, key, key);
        }
    }

    private void drawLine(Graphics2D g2d, Point startPoint, Point endPoint) {
        g2d.drawLine(startPoint.x - horizontalOffset,
                startPoint.y - verticalOffset,
                endPoint.x - horizontalOffset,
                endPoint.y - verticalOffset);
    }

    private void drawText(Graphics2D g2d, Point startPoint, String text, boolean drawBackground, Ellipse2D oval){
        if (drawBackground) {
            oval.setFrame(startPoint.x - horizontalOffset - 3, startPoint.y - verticalOffset - 10, 30, 30);
            g2d.fill(oval);
            platformIndexes.add(oval);
        }
        g2d.setColor(Color.green);
        g2d.drawString(text, startPoint.x - horizontalOffset + textOffset,
                startPoint.y - verticalOffset + textOffset);
    }

    // ---------------------- Helping functions ------------------------------
    private Point getRoundedPoint(Point point) {
        Point newPoint = new Point();
        newPoint.x = getRoundedCoordinate(point.x);
        newPoint.y = getRoundedCoordinate(point.y);
        inverseScale.transform(newPoint, newPoint);
        return newPoint;
    }

    private int getRoundedCoordinate(int coordinate) {
        int newCoordinate;
        int modifier = coordinate % roundTo;
        if (modifier == 0)
            newCoordinate = coordinate;
        else {
            if (modifier >= roundTo / 2) {
                newCoordinate = coordinate + (roundTo - modifier);
            } else {
                newCoordinate = coordinate - modifier;
            }
        }
        return  newCoordinate;
    }

    private boolean findPointInPolygon(Point point) {
        for (int i = 0; i < polygons.size(); i++) {
            Platform polygon = polygons.get(i);
            if (polygon.hasPoint(point)) {
                int pointIndex = polygon.getPointIndex(point);
                selectedPoint = polygon.getPoint(pointIndex);
                currentPolygonIndex = i;
                return true;
            }
        }
        return false;
    }

    public void resetFieldSettings(Properties properties) {
        FieldSettings settings = properties.getFieldSettings();
        setFieldSize(settings.getFieldWidth(), settings.getFieldHeight(), settings.getScaleFactor());
        setRoundTo(settings.getRoundTo());
        setShowVertexIndex(settings.showVertexesIndex());
        setShowPlatformIndexes(settings.isShowPlatformIndex());
        polygons = properties.getPlatformList();
        if (scaleFactor != settings.getScaleFactor()) {
            int oldScaleFactor = scaleFactor;
            int newScaleFactor = settings.getScaleFactor();
            for (Platform polygon : polygons) {
                polygon.scalePolygon(scaleFactor, settings.getScaleFactor());
            }

            for (Ellipse2D oval : platformIndexes) {
                oval.setFrame(oval.getX() * oldScaleFactor / newScaleFactor,
                        oval.getY() * oldScaleFactor / newScaleFactor,
                        oval.getWidth() * oldScaleFactor / newScaleFactor,
                        oval.getHeight() * oldScaleFactor / newScaleFactor);
            }

            if (selectedPoint != null)
                selectedPoint.setLocation(selectedPoint.getX() * oldScaleFactor / newScaleFactor,
                        selectedPoint.getY() * oldScaleFactor / newScaleFactor);

            if (mousePosition != null)
                mousePosition.setLocation(mousePosition.getX() * oldScaleFactor / newScaleFactor,
                        mousePosition.getY() * oldScaleFactor / newScaleFactor);

            for (Point key : keys)
                key.setLocation(key.getX() * oldScaleFactor / newScaleFactor,
                        key.getY() * oldScaleFactor / newScaleFactor);

        }
        repaint();
    }

    public void setStartPoint(MouseEvent e) {
        Point point = getRoundedPoint(e.getPoint());
        keys.add(point);
        repaint();
        System.out.println("keys " + keys.size());
    }
}
